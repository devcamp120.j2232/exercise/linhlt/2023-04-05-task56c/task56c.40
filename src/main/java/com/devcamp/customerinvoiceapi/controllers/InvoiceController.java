package com.devcamp.customerinvoiceapi.controllers;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.customerinvoiceapi.models.Invoice;
import com.devcamp.customerinvoiceapi.services.InvoiceService;

@RestController
@RequestMapping("/")
@CrossOrigin
public class InvoiceController {
    @Autowired
    InvoiceService invoiceService;
    @GetMapping("/invoices")
    public ArrayList<Invoice> getAllInvoiceApi(){
        return invoiceService.getAllInvoices();
    }
}
