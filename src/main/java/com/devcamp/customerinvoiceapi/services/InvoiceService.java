package com.devcamp.customerinvoiceapi.services;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

import com.devcamp.customerinvoiceapi.models.Invoice;
@Service
public class InvoiceService extends CustomerService{
    Invoice invoice1 = new Invoice(1001, customer1, 1000000 );
    Invoice invoice2 = new Invoice(1002, customer2, 500000 );
    Invoice invoice3 = new Invoice(1003, customer3, 450000 );
    public ArrayList<Invoice> getAllInvoices(){
        ArrayList<Invoice> invoiceList = new ArrayList<>();
        invoiceList.add(invoice1);
        invoiceList.add(invoice2);
        invoiceList.add(invoice3);
        return invoiceList;
    }
}
